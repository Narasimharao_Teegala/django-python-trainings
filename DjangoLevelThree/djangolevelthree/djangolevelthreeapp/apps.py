from django.apps import AppConfig


class DjangolevelthreeappConfig(AppConfig):
    name = 'djangolevelthreeapp'
