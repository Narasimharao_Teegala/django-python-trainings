from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
	json_text = {'insert_me': 'Hii I am in views.py file'}
	return render(request, 'first_app/index.html', context=json_text)

def help(request):
	json_text = {'insert_me': 'can you please help me to resolve this issue.'}
	return render(request, 'first_app/help.html', context=json_text)