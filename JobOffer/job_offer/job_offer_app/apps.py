from django.apps import AppConfig


class JobOfferAppConfig(AppConfig):
    name = 'job_offer_app'
