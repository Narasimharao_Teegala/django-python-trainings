from django.urls import path, re_path
from . import views

app_name = 'job_offer_app'
urlpatterns = [
    path('job_offers/', views.JobOfferInfoListView.as_view(), name = "job_offer_list"),
    re_path(r'^job_offer/(?P<pk>\d+)/$', views.JobOfferInfoDetailView.as_view(), name = "job_offer_detail"),
    path('job_offer/create/', views.JobOfferInfoCreateView.as_view(), name = "job_offer_create"),
    re_path(r'^job_offer/update/(?P<pk>\d+)/$', views.JobOfferInfoUpdateView.as_view(), name = "job_offer_update"),
    re_path(r'^job_offer/delete/(?P<pk>\d+)/$', views.JobOfferInfoDeleteView.as_view(), name = "job_offer_delete"),
    path('users/', views.UserProfileInfoListView.as_view(), name = "users_list"),
    re_path(r'^users/(?P<pk>\d+)/$', views.UserProfileInfoDetailView.as_view(), name = "user_detail"),
    path('users/create/', views.UserProfileInfoCreateView.as_view(), name = "user_create"),
    re_path(r'^users/update/(?P<pk>\d+)/$', views.UserProfileInfoUpdateView.as_view(), name = "user_update"),
    re_path(r'^users/delete/(?P<pk>\d+)/$', views.UserProfileInfoDeleteView.as_view(), name = "user_delete")
]
