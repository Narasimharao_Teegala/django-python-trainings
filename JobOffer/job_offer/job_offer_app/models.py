from django.db import models
from django.urls import reverse

# Create your models here.
class UserProfileInfo(models.Model):
    firstname = models.CharField(max_length=256, blank=True, null=True)
    lastname = models.CharField(max_length = 256, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    password = models.CharField(max_length=256, blank=True, null=True)
    experience = models.FloatField(blank=False, null=False)
    resume = models.FileField(upload_to = 'resumes', blank=True)
    role = models.CharField(max_length = 256, blank=False, null=False)
    current_company = models.CharField(max_length=256)
    technology_stack = models.CharField(max_length=120)

    def __str__(self):
        return '%s %s' %(self.firstname, self.lastname)

    def get_absolute_url(self):
        return reverse("job_offer_app:user_detail", kwargs={"pk": self.pk})

class JobOfferInfo(models.Model):
    user_profile_info = models.ManyToManyField(UserProfileInfo, through='UserJobOffer',  related_name='users')
    company = models.CharField(max_length=20, blank=False, null=False)
    designation = models.CharField(max_length=20, blank=False, null=False)
    package = models.CharField(max_length=20)
    experience = models.CharField(max_length=30)
    technology_stack = models.CharField(max_length=50)

    def __str__(self):
        return '%s %s' % (self.company, self.designation)

    def get_absolute_url(self):
        return reverse("job_offer_app:job_offer_detail", kwargs={"pk": self.pk})

class UserJobOffer(models.Model):
    user_profile_info_id = models.ForeignKey(UserProfileInfo, on_delete=models.CASCADE)
    job_offer_info_id = models.ForeignKey(JobOfferInfo, on_delete=models.CASCADE)

