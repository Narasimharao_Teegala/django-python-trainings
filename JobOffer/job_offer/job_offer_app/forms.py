from django import forms
from .models import UserProfileInfo

class UserProfileInfoForm(forms.ModelForm):
    password = forms.CharField(widget = forms.PasswordInput())
    
    class Meta():
        model = UserProfileInfo
        fields = '__all__'
