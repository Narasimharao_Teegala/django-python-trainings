# Generated by Django 3.0.4 on 2020-03-11 11:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='JobOfferInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company', models.CharField(max_length=20)),
                ('designation', models.CharField(max_length=20)),
                ('package', models.CharField(max_length=20)),
                ('experience', models.CharField(max_length=30)),
                ('technology_stack', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfileInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('experience', models.FloatField()),
                ('resume', models.FileField(blank=True, upload_to='resumes')),
                ('role', models.CharField(max_length=256)),
                ('current_company', models.CharField(max_length=256)),
                ('technology_stack', models.CharField(max_length=120)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserJobOffer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job_offer_info_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='job_offer_app.JobOfferInfo')),
                ('user_profile_info_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='job_offer_app.UserProfileInfo')),
            ],
        ),
        migrations.AddField(
            model_name='jobofferinfo',
            name='user_profile_info',
            field=models.ManyToManyField(related_name='users', through='job_offer_app.UserJobOffer', to='job_offer_app.UserProfileInfo'),
        ),
    ]
