from django.shortcuts import render
from django.views.generic import View, TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from . import models
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .forms import UserProfileInfoForm

# Create your views here.

class IndexView(TemplateView):
    template_name = 'index.html'

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['inject_me'] = 'I have injected here'
#         return context

class JobOfferInfoListView(ListView):
    context_object_name = 'job_offers' #default it will return lowercase model name underscore list i.e job_offer_info_list
    model = models.JobOfferInfo

class JobOfferInfoDetailView(DetailView):
    context_object_name = 'job_offer_info_detail' # default it will return lowercase model name i.e jobofferinfo
    model = models.JobOfferInfo
    template_name = 'job_offer_app/jobofferinfo_detail.html'

class JobOfferInfoCreateView(CreateView):
    fields = ('company', 'designation', 'package', 'experience', 'technology_stack')
    model = models.JobOfferInfo

class JobOfferInfoUpdateView(UpdateView):
    fields = ('designation', 'package', 'experience', 'technology_stack')
    model = models.JobOfferInfo

class JobOfferInfoDeleteView(DeleteView):
    model = models.JobOfferInfo
    success_url = reverse_lazy("job_offer_app:job_offer_list")

#Users CRUD

class UserProfileInfoListView(ListView):
    context_object_name = 'users' #default it will return lowercase model name underscore list i.e userprofileinfo_list
    model = models.UserProfileInfo

class UserProfileInfoDetailView(DetailView):
    context_object_name = 'user_detail' # default it will return lowercase model name i.e userprofileinfo
    model = models.UserProfileInfo
    template_name = 'job_offer_app/userprofileinfo_detail.html'

class UserProfileInfoCreateView(CreateView):
    form_class = UserProfileInfoForm
    template_name = 'job_offer_app/userprofileinfo_form.html'
    success_url = reverse_lazy('job_offer_app:users_list')

class UserProfileInfoUpdateView(UpdateView):
    fields = ('firstname', 'lastname', 'email', 'experience', 'role', 'current_company', 'technology_stack')
    template_name = 'job_offer_app/userprofileinfo_form.html'
    model = models.UserProfileInfo

class UserProfileInfoDeleteView(DeleteView):
    model = models.UserProfileInfo
    success_url = reverse_lazy("job_offer_app:users_list")
