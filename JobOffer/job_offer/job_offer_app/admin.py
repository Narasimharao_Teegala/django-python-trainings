from django.contrib import admin
from .models import UserProfileInfo, JobOfferInfo, UserJobOffer

# Register your models here.
admin.site.register(UserProfileInfo)
admin.site.register(JobOfferInfo)
admin.site.register(UserJobOffer)