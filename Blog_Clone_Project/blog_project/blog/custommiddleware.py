from datetime import datetime
from django.utils.deprecation import MiddlewareMixin


class CustomMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request._request_time = datetime.now()
        print("****************************************************************")
        print("request ", request)
        print("process request", datetime.now())
        #print("user ", request.user)
        print("****************************************************************")

    def process_view(self, request, view_func, *view_args, **view_kwargs):
        print("****************************************************************")
        print("request ", request)
        print("view_func", view_func)
        print("view_args", view_args)
        print("view_kwargs", view_kwargs)
        print("process view", datetime.now())
        #print("user ", request.user)
        print("****************************************************************")

    def process_response(self, request, response):
        response_time = request._request_time - datetime.now()
        response._response_time = abs(response_time)
        print("****************************************************************")
        print("response ", response)
        print("process response", response_time)
        #print("user ", request.user)
        print("****************************************************************")
        return response

    def process_template_response(self, request, response):
        response_time = request._request_time - datetime.now()
        response._response_time = abs(response_time)
        print("****************************************************************")
        print("response ", response)
        print("process template response", abs(response_time))
        #print("user ", request.user)
        print("****************************************************************")
        return response