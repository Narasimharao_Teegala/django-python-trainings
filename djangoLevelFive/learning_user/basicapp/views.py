from django.shortcuts import render
from .forms import UserForm, UserProfileInfoForm
from django.template.loader import get_template

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse 
# Create your views here.

def index(request):
    return render(request, 'basicapp/index.html')

def about(request):
    template = get_template('basicapp/about.html')
    return HttpResponse(template.render({}, request))

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data = request.POST)
        user_profile_info_form = UserProfileInfoForm(data = request.POST)

        if user_form.is_valid() and user_profile_info_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = user_profile_info_form.save(commit = False)
            profile.user = user
            if 'profile_pic' in request.FILES:
                profile.profile_pic = request.FILES['profile_pic']
            profile.save()
            registered = True
        else:
            print(user_form.errors, user_profile_info_form.errors)
    else:
        user_form = UserForm()
        user_profile_info_form = UserProfileInfoForm()
    template = get_template('basicapp/registration.html')
    return HttpResponse(template.render({'user_form': user_form, 'user_profile_info_form': user_profile_info_form, 'registered': registered}, request))

def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password = password)

        if user:
            if user.is_active:
                login(request, user)
                request.session['user_id'] = user.id
                return HttpResponseRedirect(reverse(index))
            else:
                return HttpResponse('Account not active')
        else:
            print("someone tried to login into your account")
            print("username:{} password:{}".format(username, password))
            return HttpResponse('Invalid login details supplied')
    else:
        return render(request, 'basicapp/login.html',{})

@login_required
def user_logout(request):
    try:
        logout(request)
        del request.session['user_id']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse(index))

@login_required
def special(request):
    return HttpResponse("Your logged into this page.")
