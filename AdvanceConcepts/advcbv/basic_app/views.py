from django.shortcuts import render
from django.views.generic import View, TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from . import models
from django.urls import reverse_lazy
from .forms import CreateStudentForm, UpdateStudentForm
# Create your views here.
# def index(request):
#     return render(request, 'index.html')

class IndexView(TemplateView):
    template_name = 'index.html'

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['inject_me'] = 'I have injected here'
#         return context

class SchoolListView(ListView):
    context_object_name = 'schools' #default it will return lowercase model name underscore list i.e school_list
    model = models.School
    template_name = 'basic_app/school_list.html'

class SchoolDetailView(DetailView):
    context_object_name = 'school_detail' # default it will return lowercase model name i.e school
    model = models.School
    template_name = 'basic_app/school_detail.html'

class SchoolCreateView(CreateView):
    fields = ('name', 'location', 'principal')
    model = models.School

class SchoolUpdateView(UpdateView):
    fields = ('name', 'principal')
    model = models.School

class SchoolDeleteView(DeleteView):
    model = models.School
    success_url = reverse_lazy("basic_app:school_list")



class StudentListView(ListView):
    context_object_name = 'students' #default it will return lowercase model name underscore list i.e school_list
    model = models.Student
    template_name = 'basic_app/students_list.html'

class StudentDetailView(DetailView):
    context_object_name = 'student_detail' # default it will return lowercase model name i.e school
    model = models.Student
    template_name = 'basic_app/student_detail.html'

class StudentCreateView(CreateView):
    form_class = CreateStudentForm
    redirect_field_name = 'basic_app/student_detail.html'
    model = models.Student

class StudentUpdateView(UpdateView):
    form_class = UpdateStudentForm
    redirect_field_name = 'basic_app/student_detail.html'
    model = models.Student

class StudentDeleteView(DeleteView):
    model = models.Student
    success_url = reverse_lazy("basic_app:student_list")