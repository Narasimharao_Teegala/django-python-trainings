from django.db import models
from django.urls import reverse
import datetime
# Create your models here.
class School(models.Model):
    name = models.CharField(max_length = 256)
    principal = models.CharField(max_length = 256)
    location = models.CharField(max_length = 256)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("basic_app:school_detail", kwargs={"pk": self.pk})
    

class Student(models.Model):
    school = models.ForeignKey(School, related_name='students', on_delete= models.CASCADE)
    name = models.CharField(max_length = 256)
    age = models.PositiveIntegerField()
    date_of_joining = models.DateField(default = datetime.date.today())

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse("basic_app:student_detail", kwargs={"pk": self.pk})
