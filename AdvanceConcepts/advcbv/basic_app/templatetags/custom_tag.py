from django import template
register = template.Library()

@register.filter
def students_count(school):
    return school.students.count()