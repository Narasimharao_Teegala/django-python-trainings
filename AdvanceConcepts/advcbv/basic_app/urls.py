from django.urls import path, re_path
from basic_app import views

app_name = 'basic_app'
urlpatterns = [
    path('school/', views.SchoolListView.as_view(), name = "school_list"),
    re_path(r'^school/(?P<pk>\d+)/$', views.SchoolDetailView.as_view(), name = "school_detail"),
    path('school/create/', views.SchoolCreateView.as_view(), name = "school_create"),
    re_path(r'^school/update/(?P<pk>\d+)/$', views.SchoolUpdateView.as_view(), name = "school_update"),
    re_path(r'^school/delete/(?P<pk>\d+)/$', views.SchoolDeleteView.as_view(), name = "school_delete"),
    path('student/', views.StudentListView.as_view(), name = "student_list"),
    re_path(r'^student/(?P<pk>\d+)/$', views.StudentDetailView.as_view(), name = "student_detail"),
    path('student/create/', views.StudentCreateView.as_view(), name = "student_create"),
    re_path(r'^student/update/(?P<pk>\d+)/$', views.StudentUpdateView.as_view(), name = "student_update"),
    re_path(r'^student/delete/(?P<pk>\d+)/$', views.StudentDeleteView.as_view(), name = "student_delete")
]
