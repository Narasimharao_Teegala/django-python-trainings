from django import forms
from .models import Student, School


class CreateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('name', 'age', 'school')

class UpdateStudentForm(forms.ModelForm):
    
    class Meta:
        model = Student
        fields = ('name', 'age')